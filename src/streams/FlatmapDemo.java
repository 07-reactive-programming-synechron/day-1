package streams;

import java.util.Arrays;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class FlatmapDemo {

    public static void main(String[] args) {
        int[][] array = {{1,2}, {2,3}, {3,4}, {5,6}};
        //output => {1, 2, 2, 3, 3, 4, 5, 6}
        Stream<int[]> streamOfIntegerArray = Arrays.stream(array);
        // we are interested in Stream<Integer>
        IntStream intStream = streamOfIntegerArray.flatMapToInt(Arrays::stream);
        //intStream.distinct().forEach(System.out::println);
        intStream.distinct().average().ifPresent(System.out::println);

    }
}