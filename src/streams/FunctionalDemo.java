package streams;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class FunctionalDemo {

    public static void main(String[] args) {

        //Pre Java-8
        Predicate<String> isIPhone = new Predicate<String>() {
            @Override
            public boolean test(String s) {
                return s.equalsIgnoreCase("iPhone");
            }
        };

        Predicate<String> isIPhonePredicate = s -> s.equalsIgnoreCase("iPhone");

        Predicate<String> isNotIphone = isIPhonePredicate.negate();

        Function<String, Integer> strToInt = str -> str.length();

        List<String> phones = Arrays.asList("iphone", "samsung", "oppo", "vivo", "oneplus");

        Consumer<String> phoneConsumer = (phone) -> System.out.println("Phone is "+ phone);

        Stream<String> stringStream = phones
                .stream()
                .filter(isNotIphone);

        stringStream.forEach(data -> System.out.println(data));
        stringStream.forEach(data -> System.out.println(data));



        /*boolean flag = isIPhone.test("samsung");
        System.out.println(" Is it an iPhone :: "+flag);*/
    }
}